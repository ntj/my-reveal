## How to use this repo

Clone or download the repo to your computer. To view the presentation, open the index.html in a browser, either directly,
or in the command line you start a local web server. Make sure, you are in the public folder of this repo when you type

```bash
cd public
$ python -m http.server 8080
```
Then you can open the URL http://localhost:8080/ in the browser.

To make changes to the slides, you can open the folder in an editor like Notepad++, gedit, vim, Sublime Text etc.

See, how it looks like:

https://ntj.gitlab.io/my-reveal/